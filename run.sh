#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f Dockerfile ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

# read MGR, USR and PSK parameters from local .config file
reposettings="${script_dir}/minidlna.conf"
usersettings="${HOME}/.config/docker/minidlna.conf"

# shellcheck disable=SC1090
source "${reposettings}"
if [ -f "${usersettings}" ]; then
  # shellcheck disable=SC1090
  source "${usersettings}"
fi

# shellcheck disable=SC1090
source "${script_dir}"/config.txt

if [ ! -d "${DSOURCE}" ]; then
  DSOURCE="/tmp/dlna/db"
  echo "<DSOURCE> not set. Using default ${DSOURCE}"
  mkdir -p "${DSOURCE}"
fi
if [ ! -d "${ASOURCE}" ]; then
  ASOURCE="/tmp/dlna/audio"
  echo "<ASOURCE> not set. Using default ${ASOURCE}"
  mkdir -p "${ASOURCE}"
fi
if [ ! -d "${VSOURCE}" ]; then
  VSOURCE="/tmp/dlna/video"
  echo "<VSOURCE> not set. Using default ${VSOURCE}"
  mkdir -p "${VSOURCE}"
fi

#           --restart unless-stopped \

docker run -d \
           -h "${container}" \
           -p 1900:1900/udp \
           -p 8200:8200/tcp \
           -v "${ASOURCE}":/srv \
           -v "${DSOURCE}":/var/cache/"${container}" \
           --name "${container}" \
           "${image}"

docker exec -it "${container}" apk update
docker exec -it "${container}" apk upgrade

sleep 3

docker logs "${container}"

# enable for testing purposes:
docker exec -it "${container}" /bin/bash
