#!/bin/bash

chown -R minidlna:minidlna /var/cache/minidlna
mkdir /var/run/minidlna && chown -R minidlna:minidlna /var/run/minidlna

set -e

if [ "$1" = "start" ];then
	exec su-exec minidlna minidlnad -dR
fi

exec "$@"
